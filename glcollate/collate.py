#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from logging import getLogger
from re import compile as re_compile

from gitlab.v4.objects import Project, ProjectJob

from .defaults import default_project_name
from .glproxy import GitlabProxy
from .job import CollateJob
from .pipeline import CollatePipeline

logger = getLogger(__name__)


class Collate:
    def __init__(
        self,
        gitlab_url: str | None = None,
        gitlab_token_file_name: str | None = None,
        namespace: str | None = None,
        project: str | None = None,
    ):
        """
        Bind to a gitlab project in a given namespace
        :param gitlab_url: string of the gitlab server url
        :param gitlab_token_file_name: file name where a token is stored
        :param namespace: namespace of the gitlab server where the project is
        :param project: project of the gitlab server to bind
        """
        self.__gitlab_proxy = GitlabProxy(gitlab_url, gitlab_token_file_name)
        namespace = (
            self.__gitlab_proxy.user.username if namespace is None else namespace
        )
        project_name = project if project is not None else default_project_name
        self.__project = self.__build_project_object(
            self.__gitlab_proxy, namespace, project_name
        )
        logger.debug(
            "Collate object build for %s",
            self.__project.web_url,
        )

    @property
    def namespace(self) -> str:
        namespace = self.__project.namespace["path"]

        if isinstance(namespace, str):
            return namespace
        else:
            raise TypeError(f"Expected str, got {namespace!r}")

    @property
    def project_name(self) -> str:
        name = self.__project.name

        if isinstance(name, str):
            return name
        else:
            raise TypeError(f"Expected str, got {name!r}")

    @property
    def path_with_namespace(self) -> str:
        path_with_namespace = self.__project.path_with_namespace

        if isinstance(path_with_namespace, str):
            return path_with_namespace
        else:
            raise TypeError(f"Expected str, got {path_with_namespace!r}")

    def from_job(self, idn: int, exclude_retried: bool = False) -> CollateJob:
        """
        Get a CollateJob object, wrapper of the gitlab job.
        :param idn: number identifying the job
        :param exclude_retried: allow to exclude retried jobs
        :return: job wrapper
        """
        logger.debug(
            "from_job(idn=%r)",
            idn,
        )
        gl_job = self.__gitlab_proxy.get_job(self.__project, idn)
        logger.info(
            "Requested CollateJob object for %s (%s)",
            gl_job.name,
            gl_job.id,
        )
        return CollateJob(gl_job, self.project_name, exclude_retried)

    def from_pipeline(
        self, idn: int, exclude_retried: bool = False, jobs: list[str] | None = None
    ) -> CollatePipeline:
        """
        Get a CollatePipeline object, wrapper of the gitlab pipeline
        :param idn: number identifying the pipeline
        :param exclude_retried: allow to exclude retried jobs
        :param jobs: list of regex to identify jobs with whom there is interest
        :return: pipeline wrapper
        """
        logger.debug(
            "from_pipeline(idn=%r, exclude_retried=%r)",
            idn,
            exclude_retried,
        )
        gl_pipeline = self.__gitlab_proxy.get_pipeline(self.__project, idn)
        return CollatePipeline(
            self.__gitlab_proxy,
            gl_pipeline,
            self.__project,
            exclude_retried,
            jobs,
        )

    def __build_project_object(
        self, gitlab_proxy: GitlabProxy, namespace: str, project_name: str
    ) -> Project:
        """
        Build the path with namespace to the project and bind the gitlab
        project object.
        :param namespace:
        :param project_name:
        :return: None
        """
        path_with_namespace = f"{namespace}/{project_name}"
        project = self.__gitlab_proxy.get_project(path_with_namespace)
        return project
