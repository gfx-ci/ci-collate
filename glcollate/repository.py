#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


import os
from logging import getLogger
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import TYPE_CHECKING

import git

from .expectations import ExpectationFile, ExpectationsFiles
from .glproxy import GitlabProxy

if TYPE_CHECKING:
    from typing import Any

    from gitlab.v4.objects import Project


logger = getLogger(__name__)


class Repository:
    __repo: git.Repo | None = None

    def __init__(
        self,
        glproxy: GitlabProxy,
        namespace: str,
        project: Project,
        branch: str | None = None,
        clone_path: str | None = None,
    ):
        """
        Prepare a local clone of the git repository.
        :param gitlab_url: string of the gitlab server url
        :param namespace:
        :param project:
        :param branch:
        :param clone_path: reuse, if possible, and existing clone
        """
        self._gl = glproxy
        self.__namespace = namespace if namespace else glproxy.user.username
        self.__project_obj = project
        self.__branch = branch if branch else project.default_branch
        if not clone_path:
            self.__local_directory: TemporaryDirectory[str] | Path = TemporaryDirectory(
                prefix=f"tmp_{project.name}_", dir=f"{os.getcwd()}/"
            )
        else:
            directory = Path(clone_path)
            if not directory.exists() or not directory.is_dir():
                raise NotADirectoryError(
                    f"Cannot use {clone_path} as a git repository."
                )
            self.__local_directory = directory

    @property
    def local_clone(self) -> str:
        if isinstance(self.__local_directory, TemporaryDirectory):
            return self.__local_directory.name
        if isinstance(self.__local_directory, Path):
            return str(self.__local_directory.absolute())

    def clone(self) -> None:
        repo_url = self.__project_obj.http_url_to_repo
        if isinstance(self.__local_directory, TemporaryDirectory):
            logger.info(
                "Cloning at %s the repository %s (branch %s)",
                self.local_clone,
                repo_url,
                self.__branch,
            )
            self.__repo = git.Repo.clone_from(
                repo_url,
                self.local_clone,
                branch=self.__branch,
                depth=1,
            )
        elif isinstance(self.__local_directory, Path):
            try:
                tmp_repo = git.Repo(self.local_clone)
            except git.exc.InvalidGitRepositoryError:
                raise AssertionError(f"Not valid git repository in {self.local_clone}")
            if not self.__check_repo_remote(tmp_repo, repo_url):
                raise AssertionError(
                    f"Remote is not pointing to {repo_url} in {self.local_clone}"
                )
            if not self.__check_repo_branch(tmp_repo):
                raise AssertionError(
                    f"Local clone is not pointing to {self.__branch} branch"
                )
            logger.info(
                "Reuse %s as an already cloned from %s (branch %s)",
                self.local_clone,
                repo_url,
                self.__branch,
            )
            self.__repo = tmp_repo

    @staticmethod
    def __check_repo_remote(repo: git.Repo, repo_url: str) -> bool:
        for remote in repo.remotes:
            for url in remote.urls:
                if url == repo_url:
                    return True
        return False

    def __check_repo_branch(self, repo: git.Repo) -> bool:
        for branch in repo.heads:  # equivalent to branches, type bug in GitPython
            if branch.name == self.__branch:
                return True
        return False

    def create_branch_from_commit(self, branch_name: str, commit_sha: str) -> None:
        if self.__repo is None:
            raise ValueError("Repository attribute not initialized")

        try:
            self.__repo.git.checkout(commit_sha)
            self.__repo.git.checkout("-b", branch_name)
            logger.info("Created a branch from the pipeline commit %s.", commit_sha)
        except (ValueError, git.exc.GitCommandError) as exception:
            logger.warning(
                "ALERT: Not possible to switch to a branch based on the "
                "pipeline commit %s.",
                commit_sha,
                exc_info=exception,
            )

    def diff(self) -> Any:
        if self.__repo is None:
            raise ValueError("Repository attribute not initialized")

        return self.__repo.git.diff("HEAD")

    def get_expectations_contents(self, path: str, prefix: str) -> ExpectationsFiles:
        """
        Get the contents of the files that could be necessary to update the
        expectations.
        :param path: string
        :param prefix: string
        :return: three dicts with the name and content of the files
        """
        answer = ExpectationsFiles()
        for suffix in ["fails", "flakes", "skips"]:
            file_name = f"{path}/{prefix}-{suffix}.txt"
            answer.insert(
                suffix, ExpectationFile(file_name, self.get_file_content(file_name))
            )
        return answer

    def get_file_content(self, file_name: str) -> list[str]:
        """
        Return the lines in a file as a list.
        :param file_name:
        :return:
        """
        complete_file_name = f"{self.local_clone}/{file_name}"
        if os.path.exists(complete_file_name):
            with open(complete_file_name, "rt") as file_descriptor:
                return file_descriptor.readlines()
        else:
            logger.info(
                "File %s doesn't exists, creating an empty one.",
                complete_file_name,
            )
            return list()

    def apply_expectation_changes(self, expectations: ExpectationsFiles) -> None:
        if not self.__repo:
            return
        current_dir = os.getcwd()
        os.chdir(self.__repo.working_dir)
        try:
            index = self.__repo.index
            additions = []
            removals = []
            for file in expectations:
                if file.modified:
                    if file.is_empty:  # wasn't empty, but all the records removed
                        removals.append(file.file_name)
                    else:
                        with open(file.file_name, "wt") as f_descriptor:
                            f_descriptor.writelines(file.as_lines)
                        additions.append(file.file_name)
            if additions:
                index.add(additions)
            if removals:
                index.remove(removals)
        finally:
            os.chdir(current_dir)
