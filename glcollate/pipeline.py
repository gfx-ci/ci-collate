#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

import concurrent.futures
from collections import defaultdict, deque
from logging import getLogger
from re import Pattern
from re import compile as re_compile
from re import match as re_match
from typing import TYPE_CHECKING

from gitlab.v4.objects import Project, ProjectJob, ProjectPipeline

from .expectations import ExpectationCategories, build_expectations_paths
from .job import CollateJob
from .repository import Repository

if TYPE_CHECKING:
    from collections.abc import Callable, Iterable, Iterator, Mapping
    from typing import Any, TypeVar

    from .artifact import Artifact
    from .glproxy import GitlabProxy

    T = TypeVar("T")


logger = getLogger(__name__)


class CollatePipeline:
    # TODO: document methods
    def __init__(
        self,
        glproxy: GitlabProxy,
        gitlab_pipeline: ProjectPipeline,
        project: Project,
        exclude_retried: bool = False,
        jobs_filter: list[str] | None = None,
    ):
        """
        Represents a pipeline in a gitlab server providing information and an
        interface to collate the results.
        :param glproxy: Collate's proxy to Gitlab
        :param gitlab_pipeline: gitlab pipeline object to wrap
        :param project: Project where the Pipeline is defined
        :param exclude_retried: boolean to not consider job retries
        :param jobs_filter: list of regex to identify jobs with whom there is interest
        """
        self._gl: GitlabProxy = glproxy
        self.__gl_pipeline: ProjectPipeline = gitlab_pipeline
        self.__project: Project = project
        self.__exclude_retried: bool = exclude_retried
        self.__jobs_filter: list[str] | None = jobs_filter

        self.__all_jobs: defaultdict[str, CollateJob] = defaultdict(CollateJob)  # type: ignore
        self.__jobs_by_stage: defaultdict[str, deque[CollateJob]] = defaultdict(deque)
        self.__jobs_with_retries: deque[CollateJob] = deque()
        self.__unknown_expectations_jobs: list[str] = []
        self.__unhandled_duplicated: list[set[ExpectationCategories]] = []
        self.__possible_trace_jobs: list[str] = []
        self.__previous_status: str | None = None

        self.refresh()

    def __str__(self) -> str:
        return f"{self.id}"

    def __repr__(self) -> str:
        return f"CollatePipeline({self})"

    @property
    def id(self) -> int:
        if self.__gl_pipeline is None:
            raise ValueError("Pipeline not initialized")
        pipeline_id = self.__gl_pipeline.id

        if isinstance(pipeline_id, int):
            return pipeline_id
        else:
            raise TypeError(f"Expected int, got {pipeline_id!r}")

    @property
    def web_url(self) -> str:
        if self.__gl_pipeline is None:
            raise ValueError("Pipeline not initialized")
        web_url = self.__gl_pipeline.web_url

        if isinstance(web_url, str):
            return web_url
        else:
            raise TypeError(f"Expected str, got {web_url!r}")

    @property
    def status(self) -> str:
        if self.__gl_pipeline is None:
            raise ValueError("Pipeline not initialized")
        status = self.__gl_pipeline.status

        if isinstance(status, str):
            return status
        else:
            raise TypeError(f"Expected str, got {status!r}")

    @property
    def previous_status(self) -> str | None:
        return self.__previous_status

    @property
    def status_changed(self) -> bool:
        if self.__previous_status is None:
            return False
        return self.__previous_status != self.status

    @property
    def attributes(self) -> dict[str, Any]:
        if self.__gl_pipeline is None:
            raise ValueError("Pipeline not initialized")
        return self.__gl_pipeline.attributes

    @property
    def pipeline_stages(self) -> set[str]:
        """
        Query which stages are in the pipeline.
        :return: Set of stages in the pipeline.
        """
        return set(self.__jobs_by_stage.keys())

    @property
    def job_stata(self) -> set[str]:
        """
        Query about the status that the jobs in the pipeline has.
        :return: Set of job status currently in the pipeline
        """
        return set((job.status for job in self.__all_jobs.values()))

    @property
    def exclude_retried(self) -> bool:
        return self.__exclude_retried

    @exclude_retried.setter
    def exclude_retried(self, value: bool) -> None:
        self.__exclude_retried = value
        # TODO: change in this flag requires a review on __all_jobs, __jobs_by_stage, and __jobs_with_retries

    @property
    def jobs_with_retries(self) -> set[CollateJob]:
        return set(self.__jobs_with_retries)

    def refresh(self) -> None:
        """
        Refresh the information in this object based on fresh information
        from the GitLab pipeline object.
        :return:
        """
        refresh_pipeline = self._gl.get_pipeline(self.__project, self.id)
        self.__refresh_pipeline_status(refresh_pipeline.status)
        self.__gl_pipeline = refresh_pipeline
        self.__refresh_pipeline_jobs()

    def jobs(self, job_regex: str | None = None) -> set[CollateJob]:
        return set(self._iter_jobs(job_regex=job_regex))

    def jobs_in_stage(
        self, stage: str | list[str] | set[str] | None
    ) -> set[CollateJob]:
        """
        Query the jobs, filtered by belonging to a stage or a set of them.
        :param stage: a stage or a list of them
        :return: set jobs in the stages in the pipeline
        """
        stage = "*" if stage is None else stage
        return self.__query_dictionary_keys(stage, self.__jobs_by_stage)

    def jobs_in_status(
        self, status: str | list[str] | set[str] | None = None
    ) -> set[CollateJob]:
        """
        Query the jobs that have the status filtered by the argument.
        :param status: a status or a list of them
        :return: set of jobs currently in the requested status in the pipeline
        """
        status = "*" if status is None else status
        jobs_by_status: defaultdict[str, deque[CollateJob]] = defaultdict(deque)
        for job in self.__all_jobs.values():
            jobs_by_status[job.status].append(job)
        return self.__query_dictionary_keys(status, jobs_by_status)

    def trace(
        self,
        job_regex: str | None = None,
        status: str | list[str] | set[str] | None = None,
        stage: str | list[str] | set[str] | None = None,
    ) -> dict[str, dict[int, str]]:
        """
        Query the trace for a set of jobs. Many arguments allow to subset the
        jobs in the pipeline in many ways.
        :param job_regex: regular expression to filter the jobs in the pipeline
        :param status: status or a list of them to filter the jobs
        :param stage: stage or a list of them to filter the jobs
        :return: dictionary with the trace on each of the jobs
        """

        def get_trace_per_job(job: CollateJob, job_id: int) -> str:
            return job.trace(job_id)

        return self._map_jobs(
            action=get_trace_per_job,
            job_regex=job_regex,
            status=status,
            stage=stage,
        )

    # TODO: list the available artifacts

    def get_artifact(
        self,
        artifact_name: str,
        job_regex: str | None = None,
        status: str | list[str] | set[str] | None = None,
        stage: str | list[str] | set[str] | None = None,
    ) -> dict[str, dict[int, Artifact]]:
        """
        Query artifacts for a set of jobs. Many arguments allow to subset the
        jobs in the pipeline in many ways.
        :param artifact_name: string with the name
        :param job_regex: regular expression to filter the jobs in the pipeline
        :param status: status or a list of them to filter the jobs
        :param stage: stage or a list of them to filter the jobs
        :return: dictionary with the artifact on each of the jobs
        """
        # The paths in virglrenderer may have an intermediate subdirectory:
        #  results/{deqp,piglit}-{gl,gles}-{host,virt}/failures.csv
        #  So, the pattern to get them would be:
        #   - results/*/failures.csv -> results/{unsharded_job_name}/failures.csv
        #  But, if not found there, it would also check:
        #   - results/failures.csv
        logger.debug(
            "Request to get artifact %r to pipeline %s with filters "
            "job_regex=%r, status=%r, stage=%r",
            artifact_name,
            self.id,
            job_regex,
            status,
            stage,
        )

        def get_artifacts_per_job(job: CollateJob, job_id: int) -> Artifact:
            return job.get_artifact(artifact_name=artifact_name, job_id=job_id)

        return self._map_jobs(
            action=get_artifacts_per_job,
            job_regex=job_regex,
            status=status,
            stage=stage,
        )

    def expectations_update(
        self,
        local_clone: str | None = None,
        branch_namespace: str | None = None,
        patch_branch: str | None = None,
        patch_name: str | None = None,
        with_merge_request: bool = False,
        use_results_pass: bool = False,
        artifacts: list[str] | None = None,
        jobs: list[str] | None = None,
        xfiles_block_header: list[str] | None = None,
    ) -> None:
        """
        Update the expectation files and generate a patch.
        :param local_clone:
        :param branch_namespace:
        :param patch_branch:
        :param patch_name:
        :param with_merge_request:
        :param use_results_pass: flag to advice the ExpectationsProcessor to interpret Pass result
        :param artifacts: list of files used as source of expectation changes.
        :param jobs: list of regex to identify jobs for expectations update
        :param xfiles_block_header: list if strings used to identify expectations block (or create it)
        :return:
        """
        if self.__gl_pipeline is None:
            raise ValueError("Pipeline not initialized")
        repo = Repository(
            glproxy=self._gl,
            namespace=branch_namespace if branch_namespace is not None else "",
            project=self.__project,
            branch=patch_branch,
            clone_path=local_clone,
        )
        repo.clone()
        if patch_branch is None:
            patch_name = patch_name if patch_name else f"{self.id}_expectations_update"
            repo.create_branch_from_commit(patch_name, self.__gl_pipeline.sha)
        if jobs is not None:
            jobs_regex = re_compile(rf"({'|'.join(jobs)})" + r"( \d+/\d+)?")
        else:
            jobs_regex = None
        expectation = build_expectations_paths(self.__project.name)
        logger.debug(
            "Pipeline %s has %s jobs to process",
            self.id,
            len(self.__all_jobs.keys()),
        )
        for job in self.__all_jobs.values():
            if jobs_regex is not None and not jobs_regex.fullmatch(job.name):
                logger.info(
                    "Skip %s as it is excluded by the jobs argument: %s",
                    job.name,
                    jobs_regex.pattern,
                )
                continue
            if job.is_from_build_stage:
                logger.debug(
                    "Skip %s as its %s stage is from the build stages set.",
                    job.name,
                    job.stage,
                )
                continue
            if job.status not in ["success", "failed"]:
                logger.debug(
                    "Skip %s as its %s status doesn't provide "
                    "information for the expectations update",
                    job.name,
                    job.status,
                )
                continue
            try:
                target = self.__project.name.lower()
                # FIXME: it's horrible:
                if target in ["virglrenderer", "linux", "mesa"]:
                    expectations_key = job.root_name
                else:
                    raise KeyError(f"Unmanaged {target} project")
                # TODO: When the jobs have this information in the artifacts, we will only trigger the
                #  job.expectations_update(repo) method.
                expectations_key_set = set(expectation[expectations_key].keys())
                if expectations_key_set == {"path", "files"}:
                    job_expectations_path = expectation[expectations_key]["path"]
                    job_expectations_prefix = expectation[expectations_key]["files"]
                    self.__append_unhandled(
                        job.expectations_update(
                            repo,
                            job_expectations_path,
                            job_expectations_prefix,
                            use_results_pass,
                            artifacts,
                            xfiles_block_header,
                        )
                    )
                elif expectations_key_set == {"skip"}:
                    skip_reason = expectation[expectations_key]["skip"]
                    logger.info(
                        "Skip %s from expectations update because %s",
                        job.name,
                        skip_reason,
                    )
            except KeyError:
                # FIXME: this is also raised with build jobs (or non-testing jobs)
                #  and this alert may generate alarm-blindness
                logger.error(
                    "Unknown expectations for %s in %s stage",
                    job.root_name,
                    job.stage,
                )
                if job.name.count("trace"):
                    self.__possible_trace_jobs.append(job.name)
                else:
                    self.__unknown_expectations_jobs.append(job.name)
        if len(self.__possible_trace_jobs) > 0:
            logger.warning(
                "Summary: Probably traces jobs: %s",
                " ,".join(self.__possible_trace_jobs),
            )
        if len(self.__unknown_expectations_jobs) > 0:
            logger.error(
                "Summary: Jobs with unknown expectations: %s",
                " ,".join(self.__unknown_expectations_jobs),
            )
        if len(self.__unhandled_duplicated) > 0:
            # unhandled_duplicated is a list of sets, needs to convert to string before ''.join()
            unhandled_duplicated = [
                str(element) for element in self.__unhandled_duplicated
            ]
            logger.warning(
                "Summary of unhandled sets of tests results: %s.",
                " ,".join(unhandled_duplicated),
            )
        with open(f"{patch_name}.patch", "wt") as file_descriptor:
            file_descriptor.writelines(repo.diff())
        # if with_merge_request:
        #     # TODO: prepare a merge request

    def __refresh_pipeline_status(self, new_status: str) -> None:
        """
        When the ProjectPipeline is refreshed, check if the pipeline status has a change.
        :param new_status:
        :return:
        """
        previous_status: str = self.status
        if previous_status != new_status:
            self.__previous_status = previous_status
            logger.info(
                "Pipeline status changed from %s to %s", previous_status, new_status
            )
        else:
            logger.debug("Pipeline refresh without status change")
            self.__previous_status = None

    def __refresh_pipeline_jobs(self) -> None:
        """
        Parallelized iteration requesting job information from a pipeline in GitLab.
        It populates the __all_jobs dict[str,CollateJob] structure adding what's new,
        and removing if there is a retry when the flag excludes it.
        """

        def __apply_exclusion_filter(
            job_name: str, jobs_filter: Pattern  # type: ignore  # Pattern
        ) -> bool:
            if jobs_filter is not None and not jobs_filter.fullmatch(job_name):
                if job_name in self.__all_jobs.keys():
                    __remove_collate_job(self.__all_jobs.pop(job_name))
                    logger.debug(
                        "Removed previously stored %s due to filter: %s",
                        job_name,
                        jobs_filter.pattern,
                    )
                else:
                    logger.debug(
                        "Skip %s as it is excluded by filter: %s",
                        job_name,
                        jobs_filter.pattern,
                    )
                return True
            return False

        def __add_collate_job(job: CollateJob) -> None:
            self.__all_jobs[job.name] = job
            self.__jobs_by_stage[job.stage].append(job)

        def __remove_collate_job(job: CollateJob) -> None:
            if job in self.__all_jobs.values():
                self.__all_jobs.pop(job.name)
            if job in self.__jobs_by_stage[job.stage]:
                self.__jobs_by_stage[job.stage].remove(job)
            if job in self.__jobs_with_retries:
                self.__jobs_with_retries.remove(job)

        if self.__gl_pipeline is None:
            raise ValueError("Pipeline not initialized")
        interesting_jobs = None
        if self.__jobs_filter is not None:
            interesting_jobs = re_compile(
                rf"({'|'.join(self.__jobs_filter)})" + r"( \d+/\d+)?"
            )
        for pipeline_job in self.__gl_pipeline.jobs.list(
            iterator=True, include_retried=True
        ):
            if __apply_exclusion_filter(pipeline_job.name, interesting_jobs):  # type: ignore  # Pattern
                continue
            project_job: ProjectJob = ProjectJob(
                self.__project.jobs, attrs=pipeline_job.asdict(), created_from_list=True
            )
            if project_job is None:
                raise AssertionError(
                    f"Cannot bind with {self.id} pipeline in {self.web_url}"
                )
            if project_job.name not in self.__all_jobs.keys():
                __add_collate_job(
                    CollateJob(project_job, project_name=self.__project.name)
                )
            else:
                self.__all_jobs[project_job.name] |= project_job

    def _map_jobs(
        self,
        action: Callable[[CollateJob, int], T],
        job_regex: str | None = None,
        status: str | list[str] | set[str] | None = "*",
        stage: str | list[str] | set[str] | None = "*",
    ) -> dict[str, dict[int, T]]:
        """
        Filter the jobs in the pipeline by their name using a regular expression
        to then call a specific action for each of them and summarize the
        results with a dictionary structure.
        :param action: string with the name of a job object method
        :param args_list: args to the action
        :param args_dict: kwargs to the action
        :param job_regex: regular expression to filter jobs
        :param status: status of the jobs to include in the action
        :param stage: stage of the jobs to include in the action
        :return: dictionary with the requested information by job
        """

        jobs_filtered = list(self._iter_jobs(job_regex, status, stage))
        output: dict[str, dict[int, T]] = {}

        def set_result(
            per_job_output: dict[int, T],
            job: CollateJob,
            job_id: int,
        ) -> None:
            per_job_output[job_id] = self.__call_action(action, job, job_id)

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures: list[concurrent.futures.Future[None]] = []

            for job in jobs_filtered:
                per_job_output: dict[int, T] = {}
                output[job.name] = per_job_output

                if self.exclude_retried or not job.has_retries:
                    job_id = job.id
                    futures.append(
                        executor.submit(set_result, per_job_output, job, job_id)
                    )
                else:
                    for job_id in job.ids:
                        futures.append(
                            executor.submit(set_result, per_job_output, job, job_id)
                        )

            for cf in concurrent.futures.as_completed(futures):
                if (exc := cf.exception()) is not None:
                    raise exc

        return output

    def _iter_jobs(
        self,
        job_regex: str | None = None,
        status: str | list[str] | set[str] | None = "*",
        stage: str | list[str] | set[str] | None = "*",
    ) -> Iterator[CollateJob]:
        for job in self.jobs_in_status(status).intersection(self.jobs_in_stage(stage)):
            if re_match(job_regex or "", job.name):
                yield job

    @staticmethod
    def __query_dictionary_keys(
        query: str | list[str] | set[str], dct: Mapping[str, Iterable[T]]
    ) -> set[T]:
        """
        One would like to subset the jobs by one or moy keys, or even get all
        of them. This method allows to create a single list of elements with
        the jobs satisfying the condition to be in the keys of the query.
        :param query: can be a single name or a set of them
        :param dct: the dictionary from where the values are listed.
        :return: list of elements that satisfies the conditions.
        """
        if isinstance(query, str):
            if query == "*":
                lst: list[T] = []
                for value in dct.values():
                    lst += list(value)
                return set(lst)
            if query in dct.keys():
                return set(dct[query])
            raise KeyError(f"Review the available keys. {query!r} isn't there.")
        if isinstance(query, list):
            query = set(query)
        if isinstance(query, set):
            available_values = set(dct.keys())
            if not query.issubset(available_values):
                invalid = query.difference(available_values)
                raise KeyError(
                    f"Review the available keys. {invalid} "
                    f"{'is' if len(invalid) == 1 else 'are'}n't "
                    f"there."
                )
            lst = []
            for element in query:
                lst += list(dct[element])
            return set(lst)

    def __call_action(
        self,
        caller: Callable[..., Any],
        *args: Any,
        **kwargs: dict[str, Any],
    ) -> Any:
        try:
            return caller(*args, **kwargs)
        except FileNotFoundError as exception:
            return f"{exception}"
        except Exception as exception:
            logger.error(
                f"Alert! Unexpected {type(exception)} exception", exc_info=exception
            )
            raise exception

    def __append_unhandled(
        self, unhandled: list[set[ExpectationCategories]] | None
    ) -> None:
        """
        When a list of sets of duplicated tests unhandled by the job is returned,
        the information is collected to summarize them at the end.
        :param unhandled:
        :return:
        """
        if unhandled and len(unhandled):
            for results in unhandled:
                if results not in self.__unhandled_duplicated:
                    self.__unhandled_duplicated.append(results)
