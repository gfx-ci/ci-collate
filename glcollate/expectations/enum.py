# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

from enum import StrEnum


class ExpectationCategories(StrEnum):
    PASS = "Pass"
    FAIL = "Fail"
    UNEXPECTED_PASS = "UnexpectedPass"
    UNEXPECTED_IMPROVEMENT_PASS = "UnexpectedImprovement(Pass)"
    UNEXPECTED_IMPROVEMENT_SKIP = "UnexpectedImprovement(Skip)"
    UNEXPECTED_IMPROVEMENT_FAIL = "UnexpectedImprovement(Fail)"
    UNEXPECTED_IMPROVEMENT_WARN = "UnexpectedImprovement(Warn)"
    TIMEOUT = "Timeout"
    FLAKE = "Flake"
    CRASH = "Crash"
    MISSING = "Missing"
    SKIP = "Skip"
    WARN = "Warn"
    EXPECTED_FAIL = "ExpectedFail"
    KNOWN_FLAKE = "KnownFlake"
