#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

import os
from logging import getLogger

from gitlab import Gitlab
from gitlab.v4.objects import (
    CurrentUser,
    Project,
    ProjectJob,
    ProjectManager,
    ProjectPipeline,
)

from .defaults import default_gitlab_token_file_name, default_gitlab_url

logger = getLogger(__name__)


class GitlabProxy:
    def __init__(
        self,
        gitlab_url: str | None = None,
        gitlab_token_file_name: str | None = None,
    ):
        self.__gitlab_url = gitlab_url if gitlab_url is not None else default_gitlab_url
        logger.debug(
            "GitlabProxy(gitlab_url=%r, gitlab_token_file_name=%r)",
            self.__gitlab_url,
            gitlab_token_file_name,
        )
        self.__gl = self.__build_gitlab_object(
            self.__gitlab_url, gitlab_token_file_name
        )
        self._user: CurrentUser | None = None

    @property
    def gitlab_url(self) -> str:
        return self.__gitlab_url

    @property
    def user(self) -> CurrentUser:
        if self._user is not None:
            return self._user

        self.__gl.auth()
        gitlab_user = self.__gl.user
        if gitlab_user is None:
            raise ValueError("Gitlab user cannot be retrieved")

        self._user = gitlab_user
        logger.debug(
            "Bind with %s with user %s",
            self.__gitlab_url,
            gitlab_user.username,
        )
        return gitlab_user

    @property
    def projects(self) -> ProjectManager:
        return self.__gl.projects

    @staticmethod
    def __build_gitlab_object(gitlab_url: str, file_name: str | None = None) -> Gitlab:
        """
        Build the link to the gitlab server to collate information about jobs
        and pipelines
        :return: None
        """
        gitlab_token: str | None = None
        if file_name is not None:
            logger.debug(
                "Using %s to get a gitlab token",
                file_name,
            )
            gitlab_token = GitlabProxy.__get_token_file_content(file_name)
        if gitlab_token is None:
            gitlab_token = os.environ.get("GITLAB_TOKEN")
        if gitlab_token is None:
            logger.debug(
                "Environment variable GITLAB_TOKEN not set, "
                "try the default location of a gitlab token file"
            )
            gitlab_token = GitlabProxy.__get_token_file_content(
                default_gitlab_token_file_name
            )

        return GitlabProxy._gitlab_builder(url=gitlab_url, private_token=gitlab_token)

    @staticmethod
    def __get_token_file_content(name: str) -> str:
        token_file = os.path.expanduser(name)
        try:
            with open(token_file) as file_descriptor:
                return file_descriptor.read().strip()
        except FileNotFoundError:
            raise AssertionError(
                "Use the GITLAB_TOKEN environment variable or "
                "the ~/.gitlab-token to provide this information"
            )

    @staticmethod
    def _gitlab_builder(url: str, private_token: str | None = None) -> Gitlab:
        return Gitlab(
            url,
            private_token=private_token,
            retry_transient_errors=True,
            per_page=5_000,
        )

    def get_project(self, path_with_namespace: str) -> Project:
        return self.__gl.projects.get(path_with_namespace)

    def get_pipeline(self, project: Project, pipeline_id: int) -> ProjectPipeline:
        return project.pipelines.get(pipeline_id)

    def get_job(self, project: Project, job_id: int) -> ProjectJob:
        return project.jobs.get(job_id)
