#!/usr/bin/python3 -B

# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2024 Collabora Ltd"

from argparse import ArgumentParser
from logging import DEBUG, basicConfig, getLogger
from os import environ as os_environ
from pathlib import Path
from pprint import pformat
from time import sleep

from gitlab import Gitlab
from gitlab.v4.objects import Project, ProjectJob, ProjectPipeline
from responses import _recorder

from glcollate import Collate
from glcollate.pipeline import CollatePipeline

RECORD_FILE = "test_pipeline_refresh_{sequence}.yaml"


logger = getLogger("pipeline_refresh")


def pipeline_refresh(pipeline_id: int) -> int:
    step = 0
    gl_project, gl_pipeline = prepare_non_collate_objects(pipeline_id)
    gl_pipeline = wait_until_pipeline_starting_point(gl_project, gl_pipeline)
    collate_pipeline, step = record_collate_pipeline_creation(pipeline_id, step)

    trigger_build_jobs(gl_project, gl_pipeline)
    step = record_pipeline_snapshot(collate_pipeline, step)

    wait_until_test_start(gl_project, gl_pipeline)
    step = record_pipeline_snapshot(collate_pipeline, step)

    wait_until_at_least_one_test_completes(gl_project, gl_pipeline)
    step = record_pipeline_snapshot(collate_pipeline, step)

    retry_test_job(gl_project, gl_pipeline)
    step = record_pipeline_snapshot(collate_pipeline, step)

    wait_until_pipeline_finish(gl_project, gl_pipeline)
    step = record_pipeline_snapshot(collate_pipeline, step)

    return step


# section of auxiliary Gitlab Objects


def prepare_non_collate_objects(pipeline_id: int) -> tuple[Project, ProjectPipeline]:
    logger.info("Prepare NON collate objects.")
    gl = Gitlab(
        url="https://gitlab.freedesktop.org",
        private_token=os_environ["GITLAB_TOKEN"],
        retry_transient_errors=True,
        per_page=5_000,
    )
    project = gl.projects.get("virgl/virglrenderer")
    pipeline = project.pipelines.get(pipeline_id)
    return project, pipeline


def wait_until_pipeline_starting_point(
    project: Project, pipeline: ProjectPipeline
) -> ProjectPipeline:
    logger.info("Wait until pipeline reaches the starting point.")
    _pipeline_id = pipeline.id
    _status = pipeline.status
    while _status != "manual":
        logger.info(
            f"Waiting pipeline {_pipeline_id} initial status (it's on {_status} status)"
        )
        sleep(1)
        pipeline = project.pipelines.get(_pipeline_id)
        _status = pipeline.status
    logger.info(f"Pipeline {_pipeline_id} in {_status} status.")
    return pipeline


def trigger_build_jobs(project: Project, pipeline: ProjectPipeline) -> None:
    logger.info("Trigger build (and sanity) jobs.")
    trigger_jobs(
        get_jobs(
            project,
            pipeline,
            include_stages=["build", "sanity test"],
            include_stata=["manual"],
        )
    )


def get_jobs(
    project: Project,
    pipeline: ProjectPipeline,
    include_stages: list[str] | None = None,
    include_stata: list[str] | None = None,
    exclude_stata: list[str] | None = None,
) -> list[ProjectJob]:
    return [
        ProjectJob(project.jobs, attrs=_pipeline_job.asdict(), created_from_list=True)
        for _pipeline_job in pipeline.jobs.list(iterator=True)
        if (include_stages is None or _pipeline_job.stage in include_stages)
        and (include_stata is None or _pipeline_job.status in include_stata)
        and (exclude_stata is None or _pipeline_job.status not in exclude_stata)
    ]


def trigger_jobs(jobs: list[ProjectJob]) -> None:
    for _job in jobs:
        _job.play()
        logger.info(f"Triggering '{_job.name}' in '{_job.stage}'")


def wait_until_test_start(project: Project, pipeline: ProjectPipeline) -> None:
    logger.info("Wait until at least one test starts.")
    while True:
        _jobs = get_jobs(
            project,
            pipeline,
            include_stages=["test"],
            exclude_stata=["created", "skipped"],
        )
        if len(_jobs) == 0:
            sleep(10)
        else:
            jobs_str = ", ".join(f"{_job.name} {_job.status}" for _job in _jobs)
            logger.info(
                f"There is at least one job pending/running in test stage: {jobs_str}"
            )
            return


def wait_until_at_least_one_test_completes(
    project: Project, pipeline: ProjectPipeline
) -> None:
    logger.info("Wait until at least one test completes.")
    while True:
        _jobs = get_jobs(
            project,
            pipeline,
            include_stages=["test"],
            include_stata=["success", "failed"],
        )
        if len(_jobs) == 0:
            sleep(60)
        else:
            jobs_str = ", ".join(f"{_job.name} {_job.status}" for _job in _jobs)
            logger.info(
                f"There is at least one job success/failed in test stage: {jobs_str}"
            )
            return


def retry_test_job(project: Project, pipeline: ProjectPipeline) -> None:
    logger.info("Retry on single test job.")
    _jobs = get_jobs(
        project, pipeline, include_stages=["test"], include_stata=["success", "failed"]
    )
    retry_jobs([_jobs[0]])


def retry_jobs(jobs: list[ProjectJob]) -> None:
    for _job in jobs:
        _job.retry()
        logger.info(f"Retry '{_job.name}' in '{_job.stage}'")


def wait_until_pipeline_finish(project: Project, pipeline: ProjectPipeline) -> None:
    logger.info("Wait until pipeline finishes.")
    while True:
        pipeline = project.pipelines.get(pipeline.id)
        if pipeline.status in ["success", "failed"]:
            logger.info(f"Pipeline finish with {pipeline.status} status.")
            return
        logger.debug(f"Pipeline in {pipeline.status}, waiting to finish.")
        sleep(60)


# section of CollatePipeline actions to be recorded


def record_collate_pipeline_creation(
    pipeline_id: int, sequence: int
) -> tuple[CollatePipeline, int]:
    @_recorder.record(file_path=RECORD_FILE.format(sequence=sequence))  # type: ignore [misc]
    def do_record() -> CollatePipeline:
        _collate = Collate(
            gitlab_url="https://gitlab.freedesktop.org",
            namespace="virgl",
            project="virglrenderer",
        )
        pipeline = _collate.from_pipeline(pipeline_id)
        get_collate_pipeline_status(pipeline)
        _stages = get_collate_pipeline_stages(pipeline)
        get_collate_pipeline_stages_by_job(pipeline, _stages)
        _stata = get_collate_pipeline_job_stata(pipeline)
        get_collate_pipeline_status_by_job(pipeline, _stata)
        return pipeline

    logger.debug(f"Recording step {sequence}.")
    pipeline = do_record()
    logger.debug(f"Step {sequence} done.")
    return pipeline, sequence + 1


def record_pipeline_snapshot(pipeline: CollatePipeline, sequence: int) -> int:
    @_recorder.record(file_path=RECORD_FILE.format(sequence=sequence))  # type: ignore [misc]
    def do_record() -> None:
        pipeline.refresh()
        get_collate_pipeline_status(pipeline)
        _stata = get_collate_pipeline_job_stata(pipeline)
        get_collate_pipeline_status_by_job(pipeline, _stata)

    logger.debug(f"Recording step {sequence}.")
    do_record()
    logger.debug(f"Step {sequence} done.")
    return sequence + 1


def get_collate_pipeline_status(pipeline: CollatePipeline) -> None:
    _status = pipeline.status
    logger.info(f"Pipeline status: {_status}")


def get_collate_pipeline_stages(pipeline: CollatePipeline) -> set[str]:
    stages = pipeline.pipeline_stages
    logger.info(f"Jobs stages: {stages}")
    return stages


def get_collate_pipeline_stages_by_job(
    pipeline: CollatePipeline, stages: set[str]
) -> None:
    _jobs_by_stages = {stage: pipeline.jobs_in_stage(stage) for stage in stages}
    _jobs_ctr = {stage: len(jobs) for stage, jobs in _jobs_by_stages.items()}
    logger.info(f"Jobs in stages: {_jobs_ctr}")
    logger.info(f"Jobs by stages:\n{pformat(_jobs_by_stages)}")


def get_collate_pipeline_job_stata(pipeline: CollatePipeline) -> set[str]:
    stata = pipeline.job_stata
    logger.info(f"Jobs status: {stata}")
    return stata


def get_collate_pipeline_status_by_job(
    pipeline: CollatePipeline, stata: set[str]
) -> None:
    _jobs_by_status = {status: pipeline.jobs_in_status(status) for status in stata}
    _jobs_ctr = {status: len(jobs) for status, jobs in _jobs_by_status.items()}
    logger.info(f"Jobs in status: {_jobs_ctr}")
    logger.info(f"Jobs by status:\n{pformat(_jobs_by_status)}")


# final section


def post_process_yaml(n_sequences: int) -> None:
    for i in range(n_sequences):
        file = Path(RECORD_FILE.format(sequence=i))
        logger.debug(f"replace content_type in {file}")
        file.write_text(
            file.read_text().replace(
                "content_type: text/plain", "content_type: application/json"
            )
        )


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Provide the id of a virglrenderer pipeline (one can prepare a fake MR) "
        "to trace its execution to be used in a test.",
    )
    parser.add_argument(
        "--pipeline-id",
        metavar="ID",
        type=int,
        required=True,
        help="pipeline id in virgl/virglrenderer",
    )
    args = parser.parse_args()
    basicConfig(
        filename=f"{args.pipeline_id}_pipeline_refresh.log",
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
        level=DEBUG,
    )
    iterations = pipeline_refresh(args.pipeline_id)
    post_process_yaml(iterations)
