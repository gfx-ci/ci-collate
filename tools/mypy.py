#!/usr/bin/python3 -B

# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

from pathlib import Path
from subprocess import run

PROJECT_ROOT = Path(__file__).parent.parent

GLCOLLATE_DIR = PROJECT_ROOT / "glcollate"
TESTS_DIR = PROJECT_ROOT / "tests"
TESTS_EXPECTATIONS_DIR = TESTS_DIR / "expectations"
TOOLS_DIR = PROJECT_ROOT / "tools"
TOOLS_RECORDERS_DIR = TOOLS_DIR / "recorders"
CHECKED_FILES = (
    GLCOLLATE_DIR,
    TESTS_EXPECTATIONS_DIR / "test_expectations_processor.py",
    TOOLS_RECORDERS_DIR / "job_features.py",
    TOOLS_RECORDERS_DIR / "pipeline_refresh.py",
)


def main() -> None:
    run(
        args=("mypy", "--pretty", *CHECKED_FILES),
        check=True,
        cwd=PROJECT_ROOT,
    )


if __name__ == "__main__":
    main()
