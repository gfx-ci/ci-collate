# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

from pathlib import Path
from unittest import TestCase
from zlib import crc32

import responses

from glcollate import Collate

TEST_ROOT = Path(__file__).parent
RECORD_COLLATE_JOB_FEATURES_FILE = "test_collate_job_features.yaml"

deqp_gl_virt = [67963804, 67987546]
deqp_gl_host = 67963808


class TestJob(TestCase):
    def test_job_trace(self) -> None:
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(str(TEST_ROOT / "test_job_trace.yaml"))

            collate = Collate(
                gitlab_url="https://gitlab.freedesktop.org",
                gitlab_token_file_name="/dev/null",
                namespace="mesa",
                project="mesa",
            )
            job = collate.from_job(67522730)
            job_trace = job.trace()

        self.assertEqual(3451046850, crc32(job_trace.encode()))

    def test_job_artifact(self) -> None:
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(str(TEST_ROOT / "test_job_artifact.yaml"))

            collate = Collate(
                gitlab_url="https://gitlab.freedesktop.org",
                gitlab_token_file_name="/dev/null",
                namespace="mesa",
                project="mesa",
            )
            job = collate.from_job(67530917)
            job_artifact = job.get_artifact("results/failures.csv")

        self.assertEqual(3955516225, crc32(str(job_artifact).encode()))

    def test_collate_job_features(self) -> None:
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(str(TEST_ROOT / RECORD_COLLATE_JOB_FEATURES_FILE))

            collate = Collate(
                gitlab_url="https://gitlab.freedesktop.org",
                gitlab_token_file_name="/dev/null",
                namespace="virgl",
                project="virglrenderer",
            )
            main_job = collate.from_job(deqp_gl_virt[0])
            repeate_main_job = collate.from_job(deqp_gl_virt[0])
            secondary_job = collate.from_job(deqp_gl_host)
            # Check job status:
            self.assertEqual(main_job.ids, [deqp_gl_virt[0]])
            self.assertEqual(main_job.status, "success")
            self.assertIsNone(main_job.previous_status)
            self.assertFalse(main_job.status_changed)
            self.assertFalse(main_job.latest_try_changed)

            # Test __eq__ and __ne__
            # Use assertFalse and assertTrue to make sure appropriate
            # magic method is called.

            # different CollateJob objects aren't different if they
            # represent the same job name.
            self.assertEqual(main_job, repeate_main_job)
            self.assertFalse(main_job != repeate_main_job)

            # different CollateJob objects are different if they
            # represent different job names.
            self.assertNotEqual(main_job, secondary_job)
            self.assertTrue(main_job != secondary_job)

            # different CollateJob objects aren't equal if they
            # represent different job names.
            self.assertNotEqual(main_job, secondary_job)
            self.assertFalse(main_job == secondary_job)

            # refuse to concatenate two CollateJob objects that
            # doesn't belong to the same job name.
            with self.assertRaises(TypeError):
                main_job |= deqp_gl_virt[1]
            with self.assertRaises(NameError):
                main_job |= secondary_job

            # merge two CollateJob objects to store
            # them together all the ProjectJob instances
            retry_main_job = collate.from_job(deqp_gl_virt[1])
            main_job |= retry_main_job
            self.assertEqual(main_job.ids, deqp_gl_virt)
            self.assertEqual(main_job.status, "running")
            self.assertTrue(main_job.latest_try_changed)
            main_job |= collate.from_job(deqp_gl_virt[1])
            self.assertEqual(main_job.status, "success")
            self.assertEqual(main_job.previous_status, "running")
            self.assertTrue(main_job.status_changed)
            self.assertFalse(main_job.latest_try_changed)

            # check time interpretations
            self.assertEqual(
                main_job.created_at_by_id(deqp_gl_virt[0]).timestamp(), 1733790978.0
            )
            self.assertEqual(
                main_job.started_at_by_id(deqp_gl_virt[0]).timestamp(), 1733791058.0
            )
            self.assertEqual(
                main_job.finished_at_by_id(deqp_gl_virt[0]).timestamp(), 1733791901.0
            )
            self.assertEqual(
                main_job.queue_duration_by_id(deqp_gl_virt[0]).total_seconds(), 0.599725
            )
            self.assertEqual(
                main_job.duration_by_id(deqp_gl_virt[0]).total_seconds(), 842.469109
            )

            self.assertEqual(
                main_job.created_at_by_id(deqp_gl_virt[1]), main_job.created_at
            )
            self.assertEqual(
                main_job.started_at_by_id(deqp_gl_virt[1]),
                main_job.started_at,
            )
            self.assertEqual(
                main_job.finished_at_by_id(deqp_gl_virt[1]), main_job.finished_at
            )
            self.assertEqual(
                main_job.queue_duration_by_id(deqp_gl_virt[1]),
                main_job.queue_duration,
            )
            self.assertEqual(
                main_job.duration_by_id(deqp_gl_virt[1]), main_job.duration
            )

    def test_collate_job_exclude_retries(self) -> None:
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(str(TEST_ROOT / RECORD_COLLATE_JOB_FEATURES_FILE))

            collate = Collate(
                gitlab_url="https://gitlab.freedesktop.org",
                gitlab_token_file_name="/dev/null",
                namespace="virgl",
                project="virglrenderer",
            )

            main_job = collate.from_job(deqp_gl_virt[0], exclude_retried=True)
            repeate_main_job = collate.from_job(deqp_gl_virt[0])
            secondary_job = collate.from_job(deqp_gl_host)
            self.assertEqual(main_job.ids, [deqp_gl_virt[0]])
            self.assertEqual(main_job.status, "success")
            self.assertIsNone(main_job.previous_status)
            self.assertFalse(main_job.status_changed)
            self.assertFalse(main_job.latest_try_changed)

            main_job |= collate.from_job(deqp_gl_virt[1])
            self.assertEqual(main_job.ids, [deqp_gl_virt[1]])
            self.assertEqual(main_job.status, "running")
            self.assertIsNone(main_job.previous_status)
            self.assertFalse(main_job.status_changed)
            self.assertTrue(main_job.latest_try_changed)

            main_job |= collate.from_job(deqp_gl_virt[1])
            self.assertEqual(main_job.ids, [deqp_gl_virt[1]])
            self.assertEqual(main_job.status, "success")
            self.assertEqual(main_job.previous_status, "running")
            self.assertTrue(main_job.status_changed)
            self.assertFalse(main_job.latest_try_changed)
