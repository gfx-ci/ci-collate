# ci-collate job subcommand

This subcommand is meant to collect information from a given job. Detailed 
information about the subcommand with `ci-collate job --help`. 

[[_TOC_]]

## collate traces

Using the token stored in the environment variable `GITLAB_TOKEN`, one can 
request in the stdout the trace of a job. For example, the job's output of
`https://gitlab.freedesktop.org/mesa/mesa/-/jobs/49215221` can be 
requested by:
```commandline
ci-collate \
    --namespace mesa \
    job \
        --trace \
        49215221
```
It didn't include `--project mesa` or 
`--gitlab-url https://gitlab.freedesktop.org/` because those are the default 
values. The default namespace is the user of the gitlab token.

The same example, but from a python code:

```python
from glcollate import Collate
trace = Collate(namespace='mesa').from_job(49215221).trace()
print(trace)
```

## collate artifacts

Another job in the same pipeline, 
`https://gitlab.freedesktop.org/mesa/mesa/-/jobs/49215223` failed and this tool
can be used to get the `failures.csv`:

```commandline
ci-collate \
    --namespace mesa \
    job \
        --artifact results/failures.csv \
        49215223
```

And from python:

```python
from glcollate import Collate
failures = Collate(namespace='mesa').from_job(49215223).get_artifact('results/failures.csv')
print(failures)
```