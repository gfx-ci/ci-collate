# ci-collate patch subcommand

This subcommand is meant to prepare an expectation update patch for a given 
pipeline. Detailed information about the subcommand with 
`ci-collate patch --help`.

[[_TOC_]]

## Generate a patch file

Using the token stored in the environment variable `GITLAB_TOKEN`, one can 
request the tool walk the jobs of a pipeline to review their `failures.csv` and
`results.csv`, then modify the expectation files for those jobs, to produce a 
patch.

With the pipeline `https://gitlab.freedesktop.org/mesa/mesa/-/pipelines/999481`
as an example to produce this patch, and a fresh clone of Mesa in the `tmp_mesa`
directory, one can call:

```commandline
ci-collate \
    --namespace mesa \
    --project mesa \
    patch \
        --local-clone tmp_mesa \
        999481
```

This will produce a file called `999481_expectations_update.patch` (unless the 
command line specifies another name with `--patch-name`) and the clone in 
`tmp_mesa` will be in a branch called `999481_expectations_update`. If the 
local clone is not set, the tool will clone it in an ephemeral directory, so 
the result will only be the patch file. 

(TODO: `--branch-namespace` and `--branch` arguments)

## Generate a merge request

(TODO)
